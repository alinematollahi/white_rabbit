<?php

class WhiteRabbit3
{
    /**
     * TODO Goto Test/WhiteRabbit3Test and write some tests that this method will fail
     * You are not allowed to change this method ;)
     */
    public function multiplyBy($amount, $multiplier){
        $estimatedResult = $amount * $multiplier;

        $guess = abs($amount-7);
        while(abs($estimatedResult - $amount) > 0.49 && $guess != 0){
            if($guess > 0.49)
                $guess = $guess / 2;
            if($amount > $estimatedResult){
                $amount = $amount - $guess;
            }
            else{
                $amount = $amount + $guess;
            }
        }
        return round($amount);
    }
}


// when $amount=7 method will fail (exept $amount=7, $multiplier=1)

$obj = new WhiteRabbit3();
var_dump($obj->multiplyBy(7,0));

$obj1 = new WhiteRabbit3();
var_dump($obj1->multiplyBy(7,-1));

$obj2 = new WhiteRabbit3();
var_dump($obj2->multiplyBy(7,2));


// when abs($estimatedResult - $amount) < 0.49 at first

$obj3 = new WhiteRabbit3();
var_dump($obj3->multiplyBy(0.75,1));

$obj4 = new WhiteRabbit3();
var_dump($obj4->multiplyBy(0.9,3));

$obj5 = new WhiteRabbit3();
var_dump($obj5->multiplyBy(0.6,0.6));

$obj6 = new WhiteRabbit3();
var_dump($obj6->multiplyBy(0.5,0.25));

// there is a rong ( - ) in result!

$obj7 = new WhiteRabbit3();
var_dump($obj7->multiplyBy(0.5,0));


