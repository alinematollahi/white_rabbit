<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){

        $coin = array(100,50,20,10,5,2,1);
        $coin_count = array(0,0,0,0,0,0,0);

        $total_count = 0;

        for ($i=0 ; $total_count<$amount ; $i++) {

            while ($total_count<$amount) {
                $total_count += $coin[$i];
                $coin_count[$i]++;
            }
            if ($total_count > $amount){
                $coin_count[$i]--;
                $total_count -= $coin[$i];
            } else {
                return array(
                    '1'   => $coin_count[6],
                    '2'   => $coin_count[5],
                    '5'   => $coin_count[4],
                    '10'  => $coin_count[3],
                    '20'  => $coin_count[2],
                    '50'  => $coin_count[1],
                    '100' => $coin_count[0]
                ); 
            }
        }
    }
}

$obj = new WhiteRabbit2();


var_dump($obj->findCashPayment(1255));